Source: lua-rings
Section: interpreters
Priority: optional
Maintainer: Enrico Tassi <gareuselesinge@debian.org>
Build-Depends: debhelper (>= 8.1.3), dh-lua
Standards-Version: 3.9.3
Homepage: https://github.com/keplerproject/rings
Vcs-Git: git://git.debian.org/git/pkg-lua/lua-rings.git
Vcs-Browser: http://git.debian.org/?p=pkg-lua/lua-rings.git

Package: lua-rings
Architecture: any
Multi-Arch: same
Pre-Depends: multiarch-support
Depends: ${shlibs:Depends}, ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Lua state creation and control library for the Lua language
 Rings is a library which provides a way to create new Lua states from within
 Lua. It also offers a simple way to communicate between the creator (master)
 and the created (slave) states.
 .
 This mechanism can be used to run chunks of code in an isolated
 Lua state, obtaining what is usually called a sandbox.

Package: lua-rings-dev
Architecture: any
Multi-Arch: same
Pre-Depends: multiarch-support
Section: libdevel
Depends: lua-rings (= ${binary:Version}), ${misc:Depends}
Provides: ${lua:Provides}
XB-Lua-Versions: ${lua:Versions}
Description: Development files for the rings library for the Lua language
 This package contains the development files of the rings Lua library,
 useful to create a statically linked binary (like a C application or a
 standalone Lua interpreter).
 .
 Documentation is also shipped within this package.
